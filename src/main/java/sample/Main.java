package sample;

//import javafx.application.Application;
//import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.stage.Stage;
import sample.data.network.ApiSource;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    private static final String sample = "Особливістю режиму правової охорони раціоналізаторських пропозицій є чіткий розподіл прав інтелектуальної власності між автором пропозиції та юридичною особою, яка її визнала.\n" +
            "Згідно із ст. 484 Цивільного Кодексу України автор раціоналізаторської пропозиції має право на добросовісне заохочення від юридичної особи, якій ця пропозиція подана. Юридична особа, яка визнала пропозицію раціоналізаторською, має право на використання цієї пропозиції у будь-якому обсязі.\n" +
            "Таким чином, авторові раціоналізаторської пропозиції належить комплекс особистих немайнових прав на пропозицію та майнове право на винагороду. У свою чергу, юридична особа, яка визнала пропозицію, володіє сукупністю майнових прав інтелектуальної власності щодо неї, у тому числі правом на її використання будь-яким способом.";

//
//    @Override
//    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
//        primaryStage.setTitle("Hello World");
//        primaryStage.setScene(new Scene(root, 300, 275));
//        primaryStage.show();
//    }


    public static void main(String[] args) {
        Runnable asyncTask = () -> {
            try {
                ApiSource apiSource = new ApiSource();
                String result = apiSource.getTextSubjects(sample);
                System.out.println(result);
            } catch (InterruptedException | IOException | URISyntaxException e) {
                e.printStackTrace();
            }
        };

        asyncTask.run();
//        launch(args);
    }
}
